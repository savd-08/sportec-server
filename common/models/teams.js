'use strict';

module.exports = function(Teams) {
    Teams.validatesUniquenessOf('nombre');
    Teams.remoteMethod(
        'sport',
        {
            accepts: [{ arg: 'deporte', type: 'string', required: true },
                { arg: 'usuario', type: 'string', required: true }],
            http: {path: '/sport', verb: 'get'},
            returns: { arg: 'team', type: 'Object', root: true },
        });
    
    Teams.sport = function (deporte, usuario, cb) {
        Teams.findOne({ where: { deporte: deporte, miembros: usuario}}, function (err, teams) {
            if (err) {
                cb(null, 'invalid');
            } else {
                cb(null, teams);
            }
        });
    };
};
