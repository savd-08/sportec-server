'use strict';

module.exports = function (Users) {
    Users.validatesUniquenessOf('email');
    Users.remoteMethod(
        'login',
        {
            accepts: [{ arg: 'email', type: 'string', required: true },
            		{ arg: 'password', type: 'string', required: true }],
            returns: { arg: 'user', type: 'Object', root: true },
        });
    
    Users.login = function (email, password, cb) {
        Users.findOne({ where: { email: email, password: password } }, function (err, users) {
            if (err) {
                cb(null, 'invalid');
            } else {
                cb(null, users);
            }
        });
    };
};
