#Curso SOA4ID

### Proyecto Final

###### Estudiante

* Sergio Vargas 

###### Requerimientos
* Node.js 9.2.0
* Loopback 2.2.0
* MongoDB

###### Instalación

Se debe clonar el repositorio git.
En Linux, generalmente se puede utilizar
~~~~
https://savd-08@bitbucket.org/savd-08/sportec-server.git
~~~~
Ejecutar con slc run